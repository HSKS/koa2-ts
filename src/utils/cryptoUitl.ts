import * as crypto from "crypto"
import conf from "../config/index"

export function base64En(data: string): string {
    return Buffer.from(data).toString("base64")
}

export function base64De(data: string): string {
    return Buffer.from(data, 'base64').toString()
}

export function aesEn(data: string, signKey: string = conf.signKey): string {
    const cipher = crypto.createCipher('aes192', signKey)
    let crypted = cipher.update(data, 'utf8', 'hex')
    crypted += cipher.final('hex')
    return base64En(crypted)
}

export function aesDe(data: string, signKey: string = conf.signKey): string {
    data = base64De(data)
    const decipher = crypto.createDecipher('aes192', signKey);
    let decrypted = decipher.update(data, 'hex', 'utf8');
    decrypted += decipher.final('utf8');
    return decrypted;
}
