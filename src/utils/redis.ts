import * as Redis from "koa-redis"
import conf from "../config"
const redisCli = Redis(conf.redis).client;

redisCli.on('connect', ()=>{
    console.log('redis connect');
})

redisCli.on('error', ()=>{
    console.error('redis server require start');
})

export default redisCli