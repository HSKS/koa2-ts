interface respMsg {
    code: number
    msg: string
    err: string
    data: any
}

export function success(data?: any): respMsg {
    return {
        code: 200,
        msg: "OK",
        err: null,
        data: data
    }
}

export function fail(err?: string): respMsg {
    return {
        code: 500,
        msg: "FAIL",
        err: err,
        data: null
    }
}
