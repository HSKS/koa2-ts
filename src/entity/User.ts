import {Entity, PrimaryGeneratedColumn, Column, Index} from "typeorm"
import * as fmtDate from "../utils/formatDate"
@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number

    @Index("user_username")
    @Column({name:"username",type:"varchar",default:"",length: 60})
    username: string

    @Column({name:"password", type: "varchar", default:"", length:60})
    password: string

    @Column({
        type: "timestamp",
        name: "created_at"
    })
    createdAt: Date
}
