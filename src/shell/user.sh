#!/usr/bin/env bash

user="root"
pwd="root"
mycmd="mysql -h47.105.199.18 -P3366  -u$user -p$pwd -S /tmp/mysql.sock1"

$mycmd -e "CREATE TABLE test.cluster (
  id int NOT NULL AUTO_INCREMENT,
  cluster_label varchar(50) not null comment '集群标签',
  cluster_name varchar(50) not null comment '集群名称',
  master_url varchar(200) default '' comment '主节点URL',
  config text not null comment '集群配置信息',
  primary key (id),
  index cluster_label1 (cluster_label) using btree
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='集群信息'"