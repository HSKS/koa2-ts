interface conf {
    readonly port: number
    readonly signKey: string
    readonly redis: {
        readonly host: string
        readonly port: number
        readonly db: number
    }
}

const conf = {
    port: 3003,
    signKey: "d331Ec20A3e3578E",
    redis: {
        host: "127.0.0.1",
        port: 6379,
        db: 0,
    }
}

export default conf